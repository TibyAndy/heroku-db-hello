# TibyAndy DB (heroku-db-hello)
Aplicação para persistência de dados de `domínios` específicos para `aplicações` distintas.

É um "Hello World" de integração com o Banco de Dados PostgreSQL no Heroku.

## Features v0
- Servidor com um `ping` acessível via:
   - http://localhost:5000 (local)
   - https://tibyandy-db.herokuapp.com (público)

## Roadmap v1
- APIs para
    - Cadastro de `aplicações` (somente admin)
    - Cadastro de `domínios`
    - Cadastro de `mapas de valores` em `domínios`

- Front-end
    - Painel de usuário para cadastro de `domínios` e `valores`
    - Painel de admin (autenticado) para cadastro de `aplicações`

- Versões **local** (http://localhost:5000) e de **produção**
(https://tibyandy-db.herokuapp.com)

## Roadmap v2
- Cadastro de `usuários` por `aplicação`

- **Autenticação segura** para `usuários` e `admin`

- **Dump** de dados por `aplicação`

- Versões **local**, de **staging** e de **produção**

---

## Esboço da API RESTful v1 (JSON)
### Listagem
- GET /data/`{app}`
- GET /data/`{app}`/`{domain}`
- GET /data/`{app}`/`{domain}`/`{key}`

### Inclusão
- ***(admin)*** PUT /data/`{app}`  
  Cria uma aplicação (se ela não existir)

- POST /data/`{app}`  
  Cria UM OU MAIS domínios  
  Body: `"domainName"` ou `["domain1","domain2"...]`

- POST /data/`{app}`/`{domain}`
  Insere (não sobrescreve) UM OU MAIS pares chave-valor em um domínio  
  Body: `{"key1":value1, "key2":value2...}`

### Alteração
- PUT /data/`{app}`/`{domain}`  
  Sobrescreve os valores de um domínio  
  Body: `{"key1":value1, "key2":value2...}`

- PUT /data/`{app}`/`{domain}`/`{key}`  
  Sobrescreve o valor de UMA chave de um domínio  
  Body: `value`

### Exclusão
- ***(admin)*** DELETE /data/`{app}`

- DELETE /data/`{app}`/`{domain}`

- DELETE /data/`{app}`/`{domain}`/`{key}`

### Outros
- GET /status  
  status do servidor

- GET /  
  painel do usuário

- ***(admin)*** GET /admin  
  painel do administrador

---

## Detalhes técnicos
### Desenvolvimento
- Código desenvolvido em [NodeJS 10 (lts/dubnium)](https://nodejs.org/en/), usando [NVM](https://github.com/creationix/nvm#readme) e a IDE [VSCode](https://code.visualstudio.com/).
- Persistência de dados em [PostgreSQL embarcado no Heroku](https://www.heroku.com/postgres) ou em modo memória.
- Deploy da aplicação no [Heroku](https://www.heroku.com/developers).

### Bibliotecas
- Servidor HTTP: [Koa](https://koajs.com/) com plugins [Koa-Router](https://github.com/alexmingoia/koa-router#readme) e [Koa-Body](https://github.com/dlau/koa-body#readme)
- Conexão com BD: [Knex query builder](https://knexjs.org/)

### Execução local
```bash
nvm use

npm install

npm start # para executar em modo dev ou

heroku login # apenas uma vez
heroku local
```

### Deploy
```bash
heroku git:remote -a tibyandy-db # apenas uma vez

git push heroku master
```

### Teste de ping (v0)
```bash
curl http://localhost:5000/

{"uptime":"0h 0m 21.538s","now":"2018-12-25T23:19:47.034Z","started":"2018-12-25T23:19:25.496Z"}
```
