module.exports = {
  environment: 'dev',
  server: {
    port: 5000
  },
  db: {
    client: 'sqlite3',
    connection: { 'filename': './tmp/knex-memory-db.sqlite' }
  }
}