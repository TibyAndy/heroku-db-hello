module.exports = {
  environment: 'prod',
  db: {
    client: "pg",
    connection: process.env.PG_CONNECTION_STRING,
    searchPath: [ "knex", "public" ]
  }
}
