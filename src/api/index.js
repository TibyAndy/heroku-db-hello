const libHome = require('../lib/home')

const ping = ctx => {
  ctx.body = libHome.ping()
}

module.exports = {
  ping
}