const configFiles = {
  dev: require('../config/dev.js'),
  prod: require('../config/prod.js')
}

const configFile = configFiles[process.env.NODE_ENV] || configFiles['dev']

console.log(`[CONFIG] using [${configFile.environment}] settings`)

const configs = {
  server: {
    port: configFile.server && configFile.server.port || process.env.PORT || 3000
  },
  db: configFile.db || {}
}

module.exports = configs
