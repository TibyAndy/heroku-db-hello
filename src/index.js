const config = require('./config')
const server = require('./server')
const routes = require('./routes')

server.launch(config, routes)