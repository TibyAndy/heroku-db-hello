let _initDate = new Date()

const setInitDate = date => {
  console.log(`[LIB:HOME] Set Init Date: ${date}`)
  _initDate = date
}

const ping = () => {
  const now = new Date()
  const diff = now.getTime() - _initDate.getTime()
  const seconds = diff % 60000 / 1000
  const minutes = Math.floor(diff / 60000)
  const hours = Math.floor(diff / 3600000)
  const uptime = `${hours}h ${minutes}m ${seconds}s`
  const result = { uptime, now, started: _initDate }
  console.log(`[LIB:PING] ${JSON.stringify(result)}`)
  return result
}

module.exports = {
  ping,
  setInitDate
}