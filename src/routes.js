const api = require('./api')

const init = koaRouter => {
  koaRouter.get('/', api.ping)
}

module.exports = {
  init
}
