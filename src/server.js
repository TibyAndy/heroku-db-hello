const Koa = require('koa')
const KoaRouter = require('koa-router')
const koaBody = require('koa-body')

const libHome = require('./lib/home')

const launch = (config, routes) => {
  const app = new Koa()
  const koaRouter = new KoaRouter()
  routes.init(koaRouter)

  const { server: { port } } = config

  app
    .use(koaBody())
    .use(koaRouter.routes())
    .use(koaRouter.allowedMethods())
    .listen(port)

  libHome.setInitDate(new Date())
  console.log(`[SERVER] running on port [${port}]`)
}

module.exports = {
  launch
}
